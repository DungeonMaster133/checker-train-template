#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import inspect
import traceback
import requests
import secrets
import string
from enum import Enum

""" <config> """
# SERVICE INFO
PORT = 8083

# DEBUG -- logs to stderr, TRACE -- verbose log
DEBUG = os.getenv("DEBUG", True)
TRACE = os.getenv("TRACE", False)
""" </config> """


def check(host):
    _log(f"Running CHECK on {host}")
    try:
        requests.get(host).status_code
    except Exception:
        die(ExitStatus.DOWN, "Down")
    session = requests.Session()
    old_username = ''.join(secrets.choice(string.ascii_uppercase + string.digits)
                                                  for i in range(10))
    password = ''.join(secrets.choice(string.ascii_uppercase + string.digits)
                                                    for i in range(10))
    recipe = ''.join(secrets.choice(string.ascii_uppercase + string.digits)
                                                    for i in range(10))
    session.post(f"{host}/signup", data={'username': old_username, 'password': password})
    session.post(f"{host}/auth", data={'username': old_username, 'password': password})
    new_username = ''.join(secrets.choice(string.ascii_uppercase + string.digits)
                                                  for i in range(10))
    session.post(f"{host}/signup", data={'username': new_username, 'password': password})
    session.post(f"{host}/auth", data={'username': new_username, 'password': password})
    list = session.get(f"{host}/bar").text
    if old_username not in list:
        die(ExitStatus.MUMBLE, "Mumble")
    session.post(f"{host}/addRecipe", data={'recipe': recipe})
    session.get(f"{host}/recipes").text
    die(ExitStatus.OK, "Ok")


def put(host, flag_id, flag, vuln):
    _log(f"Running PUT on {host} with {flag_id}:{flag}")
    try:
        requests.get(host).status_code
    except Exception:
        die(ExitStatus.DOWN, "Down")
    username = ''.join(secrets.choice(string.ascii_uppercase + string.digits)
                                                  for i in range(10))
    password = ''.join(secrets.choice(string.ascii_uppercase + string.digits)
                                                    for i in range(10))
    session = requests.Session()
    session.post(f"{host}/signup", data={'username': username, 'password': password})
    session.post(f"{host}/auth", data={'username': username, 'password': password})
    session.post(f"{host}/addRecipe", data={'recipe': flag})
    print(f"{flag_id}:{username}:{password}")

    die(ExitStatus.OK, flag_id)


def get(host, flag_id, flag, vuln):
    _log(f"Running GET on {host} with {flag_id}:{flag}")
    try:
        requests.get(host).status_code
    except Exception:
        die(ExitStatus.DOWN, "Down")
    session = requests.Session()
    flag_id, username, password = flag_id.split(':')
    session.post(f"{host}/auth", data={'username': username, 'password': password})
    res = session.get(f"{host}/recipes").text
    if username in res and flag in res:
        die(ExitStatus.OK, flag_id)
    else:
        die(ExitStatus.CORRUPT, flag_id)


""" <common> """


class ExitStatus(Enum):
    OK = 101
    CORRUPT = 102
    MUMBLE = 103
    DOWN = 104
    CHECKER_ERROR = 110


def _log(obj):
    if DEBUG and obj:
        caller = inspect.stack()[1].function
        print(f"[{caller}] {obj}", file=sys.stderr, flush=True)
    return obj


def die(code: ExitStatus, msg: str):
    if msg:
        print(msg, file=sys.stderr, flush=True)
    exit(code.value)


def _main():
    action, host, *args = sys.argv[1:]

    if not host.startswith("http"):
        host = f"http://{host}:{PORT}"
    try:
        if action == "check":
            check(host)
        elif action == "put":
            flag_id, flag, vuln = args
            put(host, flag_id, flag, vuln)
        elif action == "get":
            flag_id, flag, vuln = args
            get(host, flag_id, flag, vuln)
        else:
            raise IndexError
    except IndexError:
        die(
            ExitStatus.CHECKER_ERROR,
            f"Usage: {sys.argv[0]} check|put|get IP FLAGID FLAG",
        )
    except Exception as e:
        die(
            ExitStatus.CHECKER_ERROR,
            f"Exception: {e}. Stack:\n {traceback.format_exc()}",
        )


""" </common> """

if __name__ == "__main__":
    _main()
